var app = new Vue({
  el: '#app',
  data: {
    gamePlayerID: null,
    gameID: null,
    created: null,
    gameState: "",
    players: [{ email: "Waiting for player" }, { email: "Waiting for player" }],
    ships: [],
    oppShips: [],
    wShips: [],
    turnSalvoes: [],
    salvoes: [],
    selectedShip: {
      "type": "select ship",
      "shipLocations": "",
      "borderX": false
    },
    userHits: [],
    opponentHits: [],
    totalShots: 0,
    modalArgs: {
      "headerClass": "bg-success",
      "headerMsj": "Error",
      "bodyMsj": "error",
      "showBackBtn": false,
    },
    gameFinished: false,
    fixState: false
  },
  methods: {
    getGamePlayer() {
      const searchParams = new URLSearchParams(window.location.search);
      this.gamePlayerID = searchParams.get('Gp');
    },
    getData() {
      if (!this.gameFinished) {
        $.get('/api/game_view/' + this.gamePlayerID)
          .done((data) => {
            this.gameState = data.gameState
            this.gameID = data.ID;
            this.created = data.created;
            if (data.gamePlayers.length < 2) {
              this.players[0] = data.gamePlayers[0].player;
            } else if (data.gamePlayers[0].id == this.gamePlayerID) {
              this.players = [data.gamePlayers[0].player, data.gamePlayers[1].player];
            } else {
              this.players = [data.gamePlayers[1].player, data.gamePlayers[0].player];
            }
            this.ships = data.ships || [];
            this.oppShips = data.oppShips || [];
            this.salvoes = data.salvoes;
            if (Object.entries(data.hits).length != 0) {
              this.userHits = data.hits.user.sort((a, b) => { return b.turn - a.turn }) || [];
              this.opponentHits = data.hits.opponent.sort((a, b) => { return b.turn - a.turn }) || [];
            }
            this.totalShots = data.shots;
            if (this.gameState == "WON") {
              this.gameFinished = true;
              this.showMsj('bg-success', 'YOU WON!!!!', 'Congratulations ' + app.players[0].email + '! you have defeated your opponent', true)
            }
            if (this.gameState == 'TIED') {
              this.gameFinished = true;
              this.showMsj('bg-primary', 'TIE!!!!', 'Good match ' + app.players[0].email + '! you almost managed to defeat your opponent', true)
            }
            if (this.gameState == 'LOST') {
              this.gameFinished = true;
              this.showMsj('bg-danger', 'YOU LOST!!!!', app.players[0].email + ' you have been defeated, better luck next time', true)
            }
          })
      }
    },
    getShips() {
      let ships = [];
      let shipBoard = getWidgetsData(widgets);
      shipBoard.forEach(shipWidget => {
        ships.push(this.getShip(shipWidget));
      })
      return ships;

    },
    selectShip(event) {
      let ship = ($(event.target.offsetParent).data('_gridstack_node'))
      this.selectedShip = this.getShip(ship);
      this.selectedShip.type = event.target.offsetParent.id;
      relocateBtn(ship);
    },
    rotate() {
      let el = '#' + this.selectedShip.type;
      let width = Number($(el).attr('data-gs-width'));
      let height = Number($(el).attr('data-gs-height'));
      let x = Number($(el).attr('data-gs-x'));
      let y = Number($(el).attr('data-gs-y'));
      let v = (height > width);
      if (v && grid.isAreaEmpty(x + 1, y, height - 1, width) && x + height - 1 < 10) {
        grid.update($(el), x, y, height, width);
      } else if (!v && grid.isAreaEmpty(x, y + 1, height, width - 1) && y + width - 1 < 10) {
        grid.update($(el), x, y, height, width);
      } else {
        $(".rotate-placeholder")
          .attr('data-gs-x', x)
          .attr('data-gs-y', y)
          .attr('data-gs-width', height)
          .attr('data-gs-height', width)
          .show(0).delay(200).hide(0).delay(200).show(0).delay(200).hide(0).delay(200).show(0).delay(200).hide(0);
      }
    },
    getShip(widget) {
      let locations = [];
      let ship = {};
      let size;
      let orientation;
      if (widget.height > widget.width) {
        size = widget.height
        orientation = "v";
      } else { size = widget.width; orientation = "h" }
      for (let i = 0; i < size; i++) {
        let position;
        if (orientation == "v") {
          position = String.fromCharCode(65 + i + widget.y) + (widget.x + 1)
        } else { position = String.fromCharCode(65 + widget.y) + (widget.x + i + 1) }
        locations.push(position);
      }
      ship.type = widget.id;
      ship.shipLocations = locations;
      return ship;
    },
    postShips() {
      //this.wShips=shipBoard;
      let json = (JSON.stringify(this.getShips()));
      $.post({
        url: "/api/games/players/" + this.gamePlayerID + "/ships", data: json, dataType: "text", contentType: "application/json"
      }).done(() => this.getData())
    },
    set_wShips(shipArray) {
      let shipData = []
      shipArray.forEach(ship => {
        let xNode = (ship.locations[0].substr(1)) - 1;
        let yNode = (ship.locations[0].substr(0).charCodeAt(0)) - 65;
        let widthNode = 1;
        let heightNode = 1;
        if (Number(ship.locations[0].substr(1)) < Number(ship.locations[1].substr(1))) {
          widthNode = ship.locations.length;
        } else { heightNode = ship.locations.length; }
        shipData.push({
          id: ship.type,
          x: xNode,
          y: yNode,
          width: widthNode,
          height: heightNode
        })
      })
      return shipData;
    },
    paintBoard() {
      this.ships.forEach(ship => {
        ship.locations.forEach(location => {
          $('#B_' + location).addClass('ship');
        });
      });
      this.salvoes.forEach(salvo => {
        if (salvo.player === this.players[0].id) {
          salvo.locations.forEach(location => {
            if (salvo.turn > this.userHits.length) {
              $('#S_' + location).addClass('bg-target').text(salvo.turn);
            } else {
              $('#S_' + location).addClass('bg-primary uShot').text(salvo.turn).removeClass('bg-target');
            }
          });
        } else salvo.locations.forEach(location => {
          if ($('#B_' + location).hasClass('ship')) {
            $('#B_' + location).addClass('bg-danger');
          } else { $('#B_' + location).addClass('bg-target').text(salvo.turn); }
        })
      });
      this.userHits.forEach(round => {
        let turn = round.turn;
        round.hitsLocations.forEach(hit => {
          $('#S_' + hit).removeClass('bg-primary').addClass('bg-hit uShot').removeClass('bg-target')
        })
      })
      this.oppShips.forEach(ship => {
        ship.locations.forEach(location => {
          $('#S_' + location).removeClass('bg-hit');
        })
      })
    },
    cleanBoard() {
      $('.bg-target').removeClass('bg-target');
    },
    addSalvo(target) {
      if ($('#' + target.id).hasClass('uShot')) {
        this.showMsj('bg-danger', 'error', "ya disparaste aqui", false)
      } else if (this.turnSalvoes.length < this.totalShots && !this.turnSalvoes.includes(target.id)) {
        $('#' + target.id).addClass("bg-target");
        this.turnSalvoes.push(target.id)
      } else if (this.turnSalvoes.includes(target.id)) {
        $('#' + target.id).removeClass("bg-target");
        let index = this.turnSalvoes.indexOf(target.id);
        this.turnSalvoes.splice(index, 1);
      } else if (this.gameState == "WAITING_OPPONENT") {
        this.showMsj('bg-danger', 'Alert', "Wait for opponent", false)
      } else if (this.gameState == 'WAITING_OPPO_FLEET') {
        this.showMsj('bg-danger', 'Alert', "wait for the opponent to deploy the fleet", false)
      } else { this.showMsj('bg-danger', 'Alert', "no more salvos", false) }
    },
    getSalvoes() {
      let salvoes = {};
      let salvoLocations = [];
      this.turnSalvoes.forEach(salvoLocation => {
        let location;
        location = salvoLocation.substr(2);
        salvoLocations.push(location)
      })
      salvoes.salvoLocations = salvoLocations;
      return salvoes
    },
    postSalvoes() {
      let json = (JSON.stringify(this.getSalvoes()));
      $.post({
        url: "/api/games/players/" + this.gamePlayerID + "/salvoes", data: json, dataType: "text", contentType: "application/json"
      }).done(() => {
        this.getData()
        app.turnSalvoes = [];
        this.cleanBoard();
      }).fail((response) => { this.showMsj('bg-danger', "Failed: ", response.responseText, false); });

    },
    shipClass(shipId) {
      let width = Number($('#' + shipId).attr('data-gs-width'));
      let height = Number($('#' + shipId).attr('data-gs-height'));
      if (height > width) {
        return shipId + 'V';
      } else { return shipId + 'H'; }
    },
    isSunk(shipType, arrayHits) {
      let shipSize;
      this.ships.forEach(ship => {
        if (ship.type == shipType) {
          shipSize = ship.locations.length;
        }
      })
      let aux = arrayHits.length;
      if (aux == 0) {
        return false;
      } else if (arrayHits[0].fleetState[shipType] == shipSize) {
        return true
      } else { return false }
    },
    showMsj(bg, header, body, btn) {
      this.modalArgs.headerClass = bg;
      this.modalArgs.headerMsj = header;
      this.modalArgs.bodyMsj = body;
      this.modalArgs.showBackBtn = btn;
      $("#modalMsg").modal("show");
    },
    refresh() {
      setInterval(() => app.getData(), 5000);
    },
    backToLobby() {
      location.href = "/web/games.html"
    },
    sortImpactList(impactList) {
      let array = [];

      if (impactList.carrier != 0) {
        array.push({ "type": "carrier", "hits": impactList.carrier })
      }
      if (impactList.submarine != 0) {
        array.push({ "type": "submarine", "hits": impactList.submarine })
      }
      if (impactList.destroyer != 0) {
        array.push({ "type": "destroyer", "hits": impactList.destroyer })
      }
      if (impactList.patrol != 0) {
        array.push({ "type": "patrol", "hits": impactList.patrol })
      }
      if (impactList.battleship != 0) {
        array.push({ "type": "battleship", "hits": impactList.battleship })
      }
      array.sort((a, b) => (a.type > b.type) ? 1 : -1)
      return array;
    },
    translate(gameState) {
      switch (gameState) {
        case 'WAITING_OPPONENT':
          return 'Waiting for opponent'
          break;
        case 'WAITING_OPPO_FLEET':
          return "wait for the opponent to deploy the fleet"
          break;
        case 'WAITING_OPPO_TURN':
          return "Waiting for the opponent turn"
          break;
        case 'YOUR_TURN':
          return "Your turn"
          break;
        case 'DEPLOY_FLEET':
          return "Deploy your fleet"
          break;
        default:
          return gameState;
          break;
      }
    },
    fixGameState() {
      var gameState = document.getElementById("gameState");
      var sticky = gameState.offsetTop;
      if (window.pageYOffset > sticky) {
        this.fixState = true;
      } else {
        this.fixState= false;
      }
    }
  },
  computed: {
    getDate() {
      return new Date(this.created).toLocaleString()
    },
    shots() {
      return this.totalShots - this.turnSalvoes.length;
    }
  },
  created() {
    this.getGamePlayer();
    this.getData();
    this.refresh();
    $(".rotate-placeholder").hide();

  },
  mounted() {
    $(".salvoPoint").click((event) => this.addSalvo(event.target)); 
    window.onscroll = function () {this.app.fixGameState();};
  },
  updated() {
    this.paintBoard();
  },
})




