var app = new Vue({
  el: "#app",
  data: {
    ships: []
  }
})
//Estas son las opciones para la biblioteca gridstack
var options = {
  width: 10, //cantidad maxima de columnas
  height: 10, //cantidad maxima de filas
  verticalMargin: 0,//mas claro imposible
  cellHeight: 45, //altura de las celdas. Por dafecto la altura de la grid es dinámica. El ancho de las celdas es determinado por la cantidad repartida en el tamaño de la grid
  disableResize: false,//Si es verdadero no es posible modificar el tamaño de los Widgets mediante la interfáz grafica
  disableOneColumnMode: false,//desabilita el modo de una columna si el ancho de la ventana es menor al ancho minimo de la grid 
  animate: true,
  resizable: false
}

$('.grid-stack').gridstack(options);
var grid = $('#grid').data('gridstack');
var widgets = $('#grid > .grid-stack-item:visible');
app.ships = getWidgetsData(widgets);

$(".grid-stack").on("dragstart", function (event, ui) {
  console.log("drag-Start")
})

$(".grid-stack").on("dragstop", function (event, ui) {
  console.log("drag-Stop " + event.target.id);
})

$('#grid').on('change', function (event, items) {
  let ships = [];
  items.forEach(item => {
    let ship = {
      shipId: item.el[0].id,
      shipWhidth: item.width,
      location: String.fromCharCode(65 + item.y) + (item.x + 1)
    }
    ships.push(ship);
  });
  console.log(ships);
  widgets = $('#grid > .grid-stack-item:visible');
  app.ships = getWidgetsData(widgets);
});

function getWidgetsData(widgetCollection) {
  let widgetsData = []
  for (widget of widgetCollection) {
    let node = $(widget).data('_gridstack_node');
    widgetsData.push({
      id: widget.id,
      x: node.x,
      y: node.y,
      width: node.width,
      height: node.height
    });
  }
  return widgetsData;
}