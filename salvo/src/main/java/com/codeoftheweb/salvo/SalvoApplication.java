package com.codeoftheweb.salvo;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class SalvoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SalvoApplication.class, args);
        //TEST//
        System.out.println("FINISH");
        Player player1 = new Player("coquito@gmail.com", "123456");
        Game game1 = new Game();
        game1.setId(1);
        Game game2= new Game();
        game2.setId(2);
        Score score1 = new Score(game2,player1,1.0D);
        Set<Score>cuco = new HashSet<>();
        cuco.add(score1);
        GamePlayer gamePlayer1 = new GamePlayer(game1, player1);
        Salvo salvo1 = new Salvo(1, Arrays.asList("D4", "C5", "H2"), gamePlayer1);
        gamePlayer1.setSalvoes(Arrays.asList((salvo1)));
        //player1.setScores(cuco);
        System.out.println(player1.getScores().stream().map(score -> score.getScore()).findFirst());
        System.out.println(player1.getScore(game1).isPresent());
        //System.out.println(player1.getScore(game2).get().getScore());
        System.out.println(player1.makePlayerDTO());
        System.out.println(gamePlayer1.getSalvoes().get(0).getTurn());
    }

    @Bean
    public PasswordEncoder encoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public CommandLineRunner initData(PlayerRepository playerRepository,
                                      GameRepository gameRepository,
                                      GamePlayerRepository gamePlayerRepository,
                                      ShipRepository shipRepository,
                                      SalvoRepository salvoRepository,
                                      ScoreRepository scoreRepository) {
        return (args) -> {
            // save a Player
            Player player1 = new Player("santiagolofiego@gmail.com",encoder().encode("123456"));
            Player player2 = new Player("raul@hotmail.com",encoder().encode("123456"));
            Player player3 = new Player("juancito@yahoo.com.ar",encoder().encode("123456"));
            Player player4 = new Player("pedrito@hotmail.com",encoder().encode("123456"));
            playerRepository.save(player1);
            playerRepository.save(player2);
            playerRepository.save(player3);
            playerRepository.save(player4);

            //save games

            Game game1 = new Game();
            Game game2 = new Game();
            Game game3 = new Game();

            game2.setCreationDate(Date.from(game1.getCreationDate().toInstant().plusSeconds(1800)));
            game3.setCreationDate(Date.from(game2.getCreationDate().toInstant().plusSeconds(1800)));
            System.out.println(game1.getCreationDate());
            System.out.println(game2.getCreationDate());
            gameRepository.save(game1);
            gameRepository.save(game2);
            gameRepository.save(game3);

            GamePlayer gamePlayer1 = new GamePlayer(game1, player1);
            GamePlayer gamePlayer2 = new GamePlayer(game1, player2);
            GamePlayer gamePlayer3 = new GamePlayer(game2, player3);
            GamePlayer gamePlayer4 = new GamePlayer(game2, player4);
            GamePlayer gamePlayer5 = new GamePlayer(game3, player3);

            gamePlayerRepository.save(gamePlayer5);
            gamePlayerRepository.save(gamePlayer1);
            gamePlayerRepository.save(gamePlayer2);
            gamePlayerRepository.save(gamePlayer3);
            gamePlayerRepository.save(gamePlayer4);

            Ship destroyer1 = new Ship("destroyer", Arrays.asList("A2", "A3", "A4", "A5"), gamePlayer1);
            Ship submarine1 = new Ship("submarine", Arrays.asList("G5", "H5", "I5", "J5"), gamePlayer1);
            Ship Battleship1 = new Ship("Battleship", Arrays.asList("A9", "B9", "C9", "D9", "E9"), gamePlayer1);
            Ship patrol1 = new Ship("Patrol Boat", Arrays.asList("I1", "I2"), gamePlayer1);
            Ship submarine2 = new Ship("submarine", Arrays.asList("F6", "G6", "H6", "I6"), gamePlayer2);
            Ship Battleship2 = new Ship("Battleship", Arrays.asList("C9", "D9", "E9", "F9", "G9"), gamePlayer2);
            Ship patrol2 = new Ship("Patrol Boat", Arrays.asList("H1", "H2"), gamePlayer2);
            Ship destroyer2 = new Ship("destroyer", Arrays.asList("D2", "D3", "D4", "D5"), gamePlayer2);

            shipRepository.save(destroyer1);
            shipRepository.save(submarine1);
            shipRepository.save(Battleship1);
            shipRepository.save(patrol1);
            shipRepository.save(destroyer2);
            shipRepository.save(submarine2);
            shipRepository.save(Battleship2);
            shipRepository.save(patrol2);

            Salvo salvo1 = new Salvo(1, Arrays.asList("D4", "C5", "H2"), gamePlayer1);
            Salvo salvo2 = new Salvo(1, Arrays.asList("H2", "A5", "G5"), gamePlayer2);
            Salvo salvo3 = new Salvo(2, Arrays.asList("D1", "C7", "H3"), gamePlayer1);
            Salvo salvo4 = new Salvo(2, Arrays.asList("D4", "A6", "J5"), gamePlayer2);

            salvoRepository.save(salvo1);
            salvoRepository.save(salvo2);
            salvoRepository.save(salvo3);
            salvoRepository.save(salvo4);

            Score score1 = new Score(game1, player1, 1.0D);
            Score score2 = new Score(game1, player2, 0.0D);

            scoreRepository.save(score1);
            scoreRepository.save(score2);
        };
    }

}

@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

    @Autowired
    PlayerRepository playerRepository;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(inputName -> {
            Player player = playerRepository.findByUserName(inputName);
            if (player != null) {
                return new User(player.getUserName(),player.getPassword(),AuthorityUtils.createAuthorityList("USER"));
            } else {
                System.out.println("no existe ese jugador! :(");
                throw new UsernameNotFoundException("Unknown user: " + inputName);
            }
        });
    }

}

@Configuration
@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/web/games.html").permitAll()
                .antMatchers("/web/games.html").permitAll()
                .antMatchers("/web/game.html*").hasAuthority("USER")
                .antMatchers("/api/game_view/*").hasAuthority("USER")
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/api/games/**").permitAll();

        http.formLogin()
                .usernameParameter("name")
                .passwordParameter("pwd")
                .loginPage("/api/login");

        http.logout().logoutUrl("/api/logout");

        // turn off checking for CSRF tokens
        http.csrf().disable();

        // if user is not authenticated, just send an authentication failure response
        http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

        // if login is successful, just clear the flags asking for authentication
        http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

        // if login fails, just send an authentication failure response
        http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

        // if logout is successful, just send a success response
        http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
        //to access h2 console with security active
        http.headers().frameOptions().disable();
    }

    private void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        }
    }
}

