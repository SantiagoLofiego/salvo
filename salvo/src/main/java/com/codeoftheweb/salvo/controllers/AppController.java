package com.codeoftheweb.salvo.controllers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repositories.*;
import org.springframework.security.core.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.codeoftheweb.salvo.util.*;

@RestController
@RequestMapping("/api")
public class AppController {
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GamePlayerRepository gamePlayerRepository;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private ShipRepository shipRepository;
    @Autowired
    private ScoreRepository scoreRepository;

    @RequestMapping("/games")
    public Map<String, Object> getGamesAll(Authentication authentication) {
        Map<String, Object> dto = new LinkedHashMap<>();

        if (Util.isGuest(authentication)) {
            dto.put("player", "guest");
        } else {
            Player player = playerRepository.findByUserName(authentication.getName());
            dto.put("player", player.makePlayerDTO());
        }

        dto.put("games", gameRepository.findAll().stream()
                .map(b -> b.makeGameDTO())
                .collect(toList()));
        dto.put("leaderBoard", leaderBoard());
        return dto;
    }

    @RequestMapping("game_view/{nn}")
    public ResponseEntity<Map<String, Object>> getGameViewByGamePlayer(@PathVariable Long nn, Authentication authentication) {
        if (Util.isGuest(authentication)) {
            return new ResponseEntity<>(Util.newMap("error", "You must be Logged In!"), HttpStatus.CREATED);
        }

        GamePlayer gamePlayer = gamePlayerRepository.findById(nn).orElse(null);
        Player player = playerRepository.findByUserName(authentication.getName());

        if (gamePlayer == null) {
            return new ResponseEntity<>(Util.newMap("error", "This game player does not exist"), HttpStatus.UNAUTHORIZED);
        }
        GamePlayer opponent = gamePlayer.getOpponent();

        if (player.getId() != gamePlayer.getPlayer().getId()) {
            return new ResponseEntity<>(Util.newMap("error", "You are not authorized to view this page"), HttpStatus.UNAUTHORIZED);
        }

        long shots = 0;
        Map<String, Object> hits = new LinkedHashMap<>();


        
        if (gamePlayer.getGame().getScores().stream().filter(score -> score.getPlayer().getId() == gamePlayer.getPlayer().getId()).findFirst().orElse(null) == null) {
          
          if (Util.getState(gamePlayer) == GameState.LOST) {
            scoreRepository.save(new Score(gamePlayer.getGame(), gamePlayer.getPlayer(), 0.0));
            scoreRepository.save(new Score(opponent.getGame(), opponent.getPlayer(), 1.0));
          }
          if (Util.getState(gamePlayer) == GameState.TIED) {
            scoreRepository.save(new Score(gamePlayer.getGame(), gamePlayer.getPlayer(), 0.5));
            scoreRepository.save(new Score(opponent.getGame(), opponent.getPlayer(), 0.5));
          }
          if (Util.getState(gamePlayer) == GameState.WON) {
            scoreRepository.save(new Score(gamePlayer.getGame(), gamePlayer.getPlayer(), 1.0));
            scoreRepository.save(new Score(opponent.getGame(), opponent.getPlayer(), 0.0));
          }
        }
        
        if (opponent.getId() != 0) {
          hits.put("user", calculateHits(gamePlayer, gamePlayer.getOpponent()));
          hits.put("opponent", calculateHits(gamePlayer.getOpponent(), gamePlayer));
        }
        
        if (Util.getState(gamePlayer)==GameState.YOUR_TURN){
            shots = gamePlayer.getShips().stream().filter(ship -> !ship.isSunk()).count();
        }
        
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("ID", gamePlayer.getGame().getId());
        dto.put("gameState", Util.getState(gamePlayer));
        dto.put("created", gamePlayer.getGame().getCreationDate());
        dto.put("gamePlayers", gamePlayer.getGame().getGamePlayers()
                .stream()
                .map(gamePlayer1 -> gamePlayer1.makeGamePlayerDTO())
                .collect(toList()));
        dto.put("ships", gamePlayer.getShips().stream()
                .map(ship -> ship.makeShipDTO())
                .collect(toList()));
        dto.put("oppShips", opponent.getShips().stream().filter(ship -> ship.isSunk())
                                    .map(ship -> ship.makeShipDTO()).collect(toList()));
        dto.put("salvoes", gamePlayer.getGame().getGamePlayers()
                .stream()
                .flatMap(gamePlayer1 -> gamePlayer1.getSalvoes()
                        .stream()
                        .map(salvo -> salvo.makeSalvoDTO()))
                .collect(toList()));
        dto.put("hits", hits);
        dto.put("shots", shots);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    private List<Map<String, Object>> leaderBoard() {

        return playerRepository.findAll()
                .stream()
                .map(player -> player.makeScoreDTO())
                .collect(Collectors.toList());


    }

    private List<Map> calculateHits(GamePlayer gp, GamePlayer opponent) {
        List<Map> hitsDto = new ArrayList<>();
        int patrolDamage = 0;
        int carrierDamage = 0;
        int destroyerDamage = 0;
        int submarineDamage = 0;
        int battleshipDamage = 0;


        for (Salvo salvo : gp.getSalvoes()) {
            long shots = salvo.getShots();
            List<String> hitsLocations = new ArrayList<>();
            Map<String, Object> turnDto = new LinkedHashMap<>();
            long turn = salvo.getTurn();
            Map<String, Integer> impacts = new LinkedHashMap<>();

            if (opponent.getSalvoes().size() < turn) {
                break;
            }

            for (Ship ship : opponent.getShips()) {
                String type = ship.getType();
                int size = ship.getShipLocations().size();
                Integer shipHits = 0;
                for (String salvoLocation : salvo.getSalvoLocations()) {

                    if (ship.isHit(salvoLocation)) {
                        hitsLocations.add(salvoLocation);
                        shipHits++;
                        if (type.equals("patrol")) {
                            patrolDamage++;
                            if (patrolDamage == size) {
                                sunkShip(ship.getId());
                            }
                        }
                        if (type.equals("submarine")) {
                            submarineDamage++;
                            if (submarineDamage == size) {
                                sunkShip(ship.getId());
                            }
                        }
                        if (type.equals("carrier")) {
                            carrierDamage++;
                            if (carrierDamage == size) {
                                sunkShip(ship.getId());
                            }
                        }
                        if (type.equals("destroyer")) {
                            destroyerDamage++;
                            if (destroyerDamage == size) {
                                sunkShip(ship.getId());
                            }
                        }
                        if (type.equals("battleship")) {
                            battleshipDamage++;
                            if (battleshipDamage == size) {
                                sunkShip(ship.getId());
                            }
                        }
                    }
                }
                impacts.put(type, shipHits);
            }
            Map<String, Object> fleetState = new LinkedHashMap<>();
            fleetState.put("patrol", patrolDamage);
            fleetState.put("carrier", carrierDamage);
            fleetState.put("destroyer", destroyerDamage);
            fleetState.put("submarine", submarineDamage);
            fleetState.put("battleship", battleshipDamage);
            long missed = shots - hitsLocations.size();
            turnDto.put("turn", turn);
            turnDto.put("hitsLocations", hitsLocations);
            turnDto.put("missed", missed);
            turnDto.put("impacts", impacts);
            turnDto.put("fleetState", fleetState);
            hitsDto.add(turnDto);
        }

        return hitsDto;
    }

    private void sunkShip(long id) {
        Ship dbShip = shipRepository.findById(id).get();
        dbShip.setSunk(true);
        shipRepository.save(dbShip);
    }


}