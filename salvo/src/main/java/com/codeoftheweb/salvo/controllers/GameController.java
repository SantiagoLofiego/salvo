package com.codeoftheweb.salvo.controllers;

import com.codeoftheweb.salvo.models.Game;
import com.codeoftheweb.salvo.models.GamePlayer;
import com.codeoftheweb.salvo.models.Player;
import com.codeoftheweb.salvo.repositories.GamePlayerRepository;
import com.codeoftheweb.salvo.repositories.GameRepository;
import com.codeoftheweb.salvo.repositories.PlayerRepository;
import com.codeoftheweb.salvo.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GameController {
  
  @Autowired
  private GameRepository gameRepository;
  @Autowired
  private GamePlayerRepository gamePlayerRepository;
  @Autowired
  private PlayerRepository playerRepository;

  @RequestMapping(value = "/games", method = RequestMethod.POST)
  public ResponseEntity<Object> newGame(Authentication authentication) {
    if (Util.isGuest(authentication)) {
      return new ResponseEntity<>("You must be Logged to create a game", HttpStatus.UNAUTHORIZED);
    }
    Player player = playerRepository.findByUserName(authentication.getName());
    Game game = gameRepository.save(new Game());
    GamePlayer gamePlayer = gamePlayerRepository.save(new GamePlayer(game, player));

    return new ResponseEntity<>(Util.newMap("gpid", gamePlayer.getId()), HttpStatus.CREATED);
  }

  @RequestMapping(value = "/game/{nn}/players", method = RequestMethod.POST)
  public ResponseEntity<Object> joinGame(@PathVariable long nn, Authentication authentication) {
    if (Util.isGuest(authentication)) {
      return new ResponseEntity<>("You must be Logged to join a game", HttpStatus.UNAUTHORIZED);
    }
    Player player = playerRepository.findByUserName(authentication.getName());
    Game game = gameRepository.getOne(nn);
    if (game == null) {
      return new ResponseEntity<>("The game does not exist ", HttpStatus.FORBIDDEN);
    }
    boolean isPlayerPresent = game.getGamePlayers().stream()
        .anyMatch(gamePlayer -> gamePlayer.getPlayer().getId() == player.getId());
    if (isPlayerPresent) {
      return new ResponseEntity<>("You already are in the game", HttpStatus.FORBIDDEN);
    }
    if (game.getGamePlayers().size() == 2) {
      return new ResponseEntity<>("The game is full", HttpStatus.FORBIDDEN);
    } else {
      GamePlayer gamePlayer = new GamePlayer(game, player);
      gamePlayerRepository.save(gamePlayer);
      return new ResponseEntity<>(Util.newMap("gpid", gamePlayer.getId()), HttpStatus.CREATED);
    }
  }

  
}
