package com.codeoftheweb.salvo.controllers;

import com.codeoftheweb.salvo.models.GamePlayer;
import com.codeoftheweb.salvo.models.Player;
import com.codeoftheweb.salvo.models.Salvo;
import com.codeoftheweb.salvo.repositories.GamePlayerRepository;
import com.codeoftheweb.salvo.repositories.GameRepository;
import com.codeoftheweb.salvo.repositories.PlayerRepository;
import com.codeoftheweb.salvo.repositories.SalvoRepository;
import com.codeoftheweb.salvo.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/api")
public class SalvoController {

    @Autowired
    private SalvoRepository salvoRepository;
    @Autowired
    private GamePlayerRepository gamePlayerRepository;
    @Autowired
    private PlayerRepository playerRepository;

    @RequestMapping(value = "/games/players/{gpid}/salvoes", method = RequestMethod.POST)
    public ResponseEntity<Object> setSalvoes(@PathVariable Long gpid, Authentication authentication,
                                             @RequestBody Salvo salvo) {
        if (Util.isGuest(authentication)) {
            return new ResponseEntity<>("You must be Logged", HttpStatus.UNAUTHORIZED);
        }
        GamePlayer gamePlayer = gamePlayerRepository.findById(gpid).orElse(null);
        Player player = playerRepository.findByUserName(authentication.getName());

        if (player.getId() != gamePlayer.getPlayer().getId()) {
            return new ResponseEntity<>("You are not authorized", HttpStatus.UNAUTHORIZED);
        }

        GamePlayer opponent = gamePlayer.getOpponent();

        if (opponent.getId() == 0) {
            return new ResponseEntity<>(("there is not opponent"), HttpStatus.CONFLICT);
        }

        if (opponent.getShips().size() == 0) {
            return new ResponseEntity<>(("wait for opponent to deploy fleet"), HttpStatus.CONFLICT);
        }

        long shots = gamePlayer.getShips().stream().filter(ship -> ship.isSunk() == false).count();
        if (Util.isGameFinished(gamePlayer)) {
            return new ResponseEntity<>("Game already finish", HttpStatus.CONFLICT);
        }

        if (gamePlayer.getSalvoes().size() > opponent.getSalvoes().size()) {
            return new ResponseEntity<>("You already play this turn", HttpStatus.CONFLICT);
        }

        if (salvo.getSalvoLocations().size() > shots) {
            return new ResponseEntity<>(("insufficient shots"), HttpStatus.CONFLICT);
        }

        if (salvo.getSalvoLocations().size() < shots) {
            return new ResponseEntity<>(("you must fire all your shots"), HttpStatus.CONFLICT);
        }

        if (gamePlayer.getSalvoes().size() <= opponent.getSalvoes().size()) {
            salvo.setTurn(gamePlayer.getSalvoes().size() + 1);
            salvo.setGamePlayer(gamePlayer);
            salvo.setShots(shots);
            salvoRepository.save(salvo);

            return new ResponseEntity<>("Salvoes launched", HttpStatus.ACCEPTED);
        }

        return new ResponseEntity<>("You already play this turn", HttpStatus.CONFLICT);
    }


}