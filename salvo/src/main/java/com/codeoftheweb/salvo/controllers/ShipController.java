package com.codeoftheweb.salvo.controllers;

import com.codeoftheweb.salvo.models.Game;
import com.codeoftheweb.salvo.models.GamePlayer;
import com.codeoftheweb.salvo.models.Player;
import com.codeoftheweb.salvo.models.Salvo;
import com.codeoftheweb.salvo.models.Ship;
import com.codeoftheweb.salvo.util.Util;
import com.codeoftheweb.salvo.repositories.GamePlayerRepository;
import com.codeoftheweb.salvo.repositories.GameRepository;
import com.codeoftheweb.salvo.repositories.PlayerRepository;
import com.codeoftheweb.salvo.repositories.ShipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ShipController {

  @Autowired
  GamePlayerRepository gamePlayerRepository;
  @Autowired
  PlayerRepository playerRepository;
  @Autowired
  ShipRepository shipRepository;

  @RequestMapping(value = "/games/players/{gpid}/ships", method = RequestMethod.POST)
  public ResponseEntity<Object> setShips(@PathVariable Long gpid, Authentication authentication,
      @RequestBody List<Ship> ships) {
    if (Util.isGuest(authentication)) {
      return new ResponseEntity<>("You must be Logged", HttpStatus.UNAUTHORIZED);
    }
    System.out.println(ships);
    GamePlayer gamePlayer = gamePlayerRepository.findById(gpid).orElse(null);
    Player player = playerRepository.findByUserName(authentication.getName());

    if (player.getId() != gamePlayer.getPlayer().getId()) {
      return new ResponseEntity<>("You are not authorized", HttpStatus.UNAUTHORIZED);
    }
    if (ships.size() < 5) {
      return new ResponseEntity<>("miss data", HttpStatus.BAD_REQUEST);
    }
    if (gamePlayer.getShips().size() > 0) {
      return new ResponseEntity<>("ships already placed", HttpStatus.FORBIDDEN);
    }
    List<String> allLocations = (ships.stream().flatMap(ship -> ship.getShipLocations().stream())
        .collect(Collectors.toList()));
    for (String loc : allLocations) {
      if (Collections.frequency(allLocations, loc) > 1) {
        return new ResponseEntity<>("Bad Locations", HttpStatus.BAD_REQUEST);
      }
    }

    ships.forEach(ship -> {
      ship.setGamePlayer(gamePlayer);
      System.out.println(ship.getShipLocations());
      shipRepository.save(ship);
    });

    return new ResponseEntity<>("Ships successfully placed", HttpStatus.ACCEPTED);
  }
}