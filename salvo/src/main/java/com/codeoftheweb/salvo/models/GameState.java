package com.codeoftheweb.salvo.models;

public enum GameState {
    WAITING_OPPONENT,
    DEPLOY_FLEET,
    WAITING_OPPO_FLEET,
    WAITING_OPPO_TURN,
    YOUR_TURN,
    WON,
    LOST,
    TIED,
    ERROR
}
