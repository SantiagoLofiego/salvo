package com.codeoftheweb.salvo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;

@Entity
public class Player {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")

    private long id;

    private String userName;

    private String password;

    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    private Set<GamePlayer> gamePlayers;

    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    private Set<Score> scores;

    public Player() {
    }

    ;

    public Player(String userName, String password) {
        this.userName = userName;
        this.password = password;
        this.scores = new LinkedHashSet<>();
    }

    public Map<String, Object> makePlayerDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", this.getId());
        dto.put("email", this.getUserName());
        return dto;
    }

    public Map<String, Object> makeScoreDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        Map<String, Double> scoreDto = new LinkedHashMap<>();
        dto.put("id", this.getId());
        dto.put("email", this.getUserName());
        dto.put("scores", scoreDto);
        scoreDto.put("won",(double)this.getScores().stream()
                .filter(score -> score.getScore() == 1.0D)
                .count());
        scoreDto.put("lost",(double)this.getScores().stream()
                .filter(score -> score.getScore()   == 0.0D)
                .count());
        scoreDto.put("tied",(double)this.getScores().stream()
                .filter(score -> score.getScore()   == 0.5D)
                .count());
        scoreDto.put("total", scoreDto.get("won") + (scoreDto.get("tied")*0.5D) );

        return dto;
    }

    public Optional<Score> getScore(Game game) {
        Optional<Score> score = this.getScores()
                .stream()
                .filter(score1 -> score1.getGame().getId() == game.getId())
                .findFirst();
        score.orElse(null);

        return score;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<GamePlayer> getGamePlayers() {
        return gamePlayers;
    }

    public void setGamePlayers(Set<GamePlayer> gamePlayers) {
        this.gamePlayers = gamePlayers;
    }

    public Set<Score> getScores() {
        return scores;
    }

    public void setScores(Set<Score> scores) {
        this.scores = scores;
    }
}
