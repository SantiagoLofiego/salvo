package com.codeoftheweb.salvo.util;

import java.util.LinkedHashMap;
import java.util.Map;

import com.codeoftheweb.salvo.models.GamePlayer;
import com.codeoftheweb.salvo.models.GameState;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;

public class Util {

  public static boolean isGuest(Authentication authentication) {
    return authentication == null || authentication instanceof AnonymousAuthenticationToken;
  }

  public static Map<String, Object> newMap(String key, Object value) {
    Map<String, Object> map = new LinkedHashMap<>();
    map.put(key, value);
    return map;
  }

  private static long countSunks(GamePlayer gp){
    return gp.getShips().stream().filter(ship -> ship.isSunk() == true).count();
  }

  public static GameState getState(GamePlayer gp){
    if (gp.getShips().size() == 0){
      return GameState.DEPLOY_FLEET;
    }
    if(gp.getGame().getGamePlayers().size()==1){
      return GameState.WAITING_OPPONENT;
    }
    if (gp.getGame().getGamePlayers().size() == 2) {
      GamePlayer opponent = gp.getOpponent();

      if(opponent.getShips().size()==0){
        return GameState.WAITING_OPPO_FLEET;
      }
      if (countSunks(gp) == gp.getShips().size() && countSunks(opponent) == opponent.getShips().size()){
        return GameState.TIED;
      }
      if (countSunks(gp) == gp.getShips().size() && countSunks(opponent) < opponent.getShips().size()){
        return GameState.LOST;
      }
      if (countSunks(gp) < gp.getShips().size() && countSunks(opponent) == opponent.getShips().size()){
        return GameState.WON;
      }
      if(gp.getSalvoes().size()==opponent.getSalvoes().size()){
        return GameState.YOUR_TURN;
      }
      if(gp.getSalvoes().size()>opponent.getSalvoes().size()){
        return GameState.WAITING_OPPO_TURN;
      }
      if (gp.getSalvoes().size()<opponent.getSalvoes().size()){
        return GameState.YOUR_TURN;
      }
    }
    return GameState.ERROR;
  }

  public static boolean isGameFinished(GamePlayer gp){
    if (getState(gp)== GameState.WON||getState(gp)==GameState.TIED||getState(gp)==GameState.LOST){
      return true;
    }else {return false;}
  }
}